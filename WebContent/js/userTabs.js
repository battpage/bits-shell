//insert JavaScript code
function getUserTabs(userN) {
	if (userN === "admin") {
		document.getElementById('userT').innerHTML = 
			'<li><a href="#tabs-1">Home</a></li>\
			<li><a href="#tabs-2">FABL</a></li>\
			<li><a href="#tabs-3">COTR</a></li>\
			<li><a href="#tabs-4">Admin</a></li>';
		
		document.getElementById('contents').innerHTML =
			'<div id="tabs-1">\
	        	<iframe name="target-iframe" src="jsp/home.jsp"></iframe>\
	        </div>\
	        <div id="tabs-2">\
	            <iframe name="target-iframe" src="jsp/fablText.jsp"></iframe>\
	        </div>\
	        <div id="tabs-3">\
	            <iframe name="target-iframe" src="jsp/cotrText.jsp"></iframe>\
	        </div>\
	        <div id="tabs-4">\
	         	<iframe name="target-iframe" src="jsp/admin.jsp"></iframe>\
	        </div>';
	}
	else {
		document.getElementById('userT').innerHTML = 
			'<li><a href="#tabs-1">Home</a></li>\
			<li><a href="#tabs-2">FABL</a></li>\
			<li><a href="#tabs-3">COTR</a></li>';
		
		document.getElementById('contents').innerHTML =
			'<div id="tabs-1">\
	        	<iframe name="target-iframe" src="jsp/home.jsp"></iframe>\
	        </div>\
	        <div id="tabs-2">\
	            <iframe name="target-iframe" src="jsp/fablText.jsp"></iframe>\
	        </div>\
	        <div id="tabs-3">\
	            <iframe name="target-iframe" src="jsp/cotrText.jsp"></iframe>\
	        </div>';
	}
	$( "#tabs" ).tabs();
};