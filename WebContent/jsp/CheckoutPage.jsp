<%@ page language="java" contentType="text/html; charset=US-ASCII"
    pageEncoding="US-ASCII" import="java.util.*, manage.user.UserUtil"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
    <title>Logout</title>
    <script src="../jQuery/jquery-1.11.1.js" type="text/javascript"></script>
  	<script src="../jQuery/jquery-ui.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="../css/BITS.css">
	
	<script type = "text/javascript">
    //insert javascript code
	</script>
</head>
<div id="header">
	<div class="bitsHeader">
		BITS Test<br/>
		BASIS Integrated Tool Suite
	</div>
</div>
<body>
<body style="background:#F6F9FC; font-family:Arial;">
	<div style="width: auto; margin: 0 auto; padding: auto 0 40px;" id=tabs>
		<ul class="tabs">
	    	<li><a href="#tabs-1">Logout</a></li>
	    </ul>
	 	<div class="tabcontents" id="contents">
		    <div id="tabs-1">
				<%
					String user = null;
					if(session.getAttribute("user") == null){
					    response.sendRedirect("../login.html");
					}
					else 
						user = (String) session.getAttribute("user");
					
					String userName = UserUtil.getUser(request.getCookies());
				%>
				<h3>Hi <%=userName %>, do you want to logout.</h3>
				<a href="../index.jsp">Back</a>
				<br><br>
				<form action="../LogoutServlet" method="post">
					<input type="submit" value="Logout" >
				</form>
	    	</div>
		</div>
	</div>
</body>
</html>