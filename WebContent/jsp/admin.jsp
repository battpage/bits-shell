<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="java.util.*, manage.user.UserUtil"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Home</title>
	<link href="../css/BITS.css" rel="stylesheet" type="text/css" />
	
	<script type = "text/javascript">
    //insert javascript code
	</script>
</head>
<body>
	<%String userName = UserUtil.getUser(request.getCookies());%>
    <b>Welcome <%=userName %></b>
    <p>
    	Admin content displayed here.<br>
    </p>
</body>
</html>