<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="java.util.*, manage.user.UserUtil"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>BITS</title>
    <script src="../jQuery/jquery-1.11.1.js" type="text/javascript"></script>
  	<script src="../jQuery/jquery-ui.js" type="text/javascript"></script>
    <link href="../css/BITS.css" rel="stylesheet" type="text/css" />
	
	<script type = "text/javascript">
    //insert javascript code
	</script>
</head>
<div id="header">
	<div class="bitsHeader">
		BITS Test<br/>
		BASIS Integrated Tool Suite
	</div>
	<div class="userID">
		<%
			String user = null;
			if(session.getAttribute("user") == null){
			    response.sendRedirect("../login.html");
			}
			else 
				user = (String) session.getAttribute("user");
		
			String userName = UserUtil.getUser(request.getCookies());
		%>
        Welcome <%=userName %><br><br>
        <a href="CheckoutPage.jsp">Logout</a>
    </div>
</div>
<body style="background:#F6F9FC; font-family:Arial;">
	<div style="width: auto; margin: 0 auto; padding: auto 0 40px;" id="tabWindow">
        <ul class="tabs">
           <li><a href="#view1">About</a></li>
       </ul>
       <div class="tabcontents" id="contents">
           <div id="view1">
               <iframe name="target-iframe" src="aboutText.jsp"></iframe>
           </div>
		</div>
	</div>
</body>
</html>