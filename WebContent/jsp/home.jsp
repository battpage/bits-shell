<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="java.io.*,java.util.*, javax.servlet.*, manage.user.UserUtil"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Home</title>
	<link href="../css/BITS.css" rel="stylesheet" type="text/css" />
	
	<script type = "text/javascript">
    //insert javascript code
	</script>
</head>
<body>
	<%
		Date date = new Date();
		String userName = UserUtil.getUser(request.getCookies());
	%>
    <b>Welcome to BITS <%=userName %></b>
    <p>
    	You <%=userName %> were successfully authenticated on <%=date %>.<br>
    </p>
</body>
</html>
