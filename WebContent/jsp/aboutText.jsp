<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Home</title>
    <link href="../css/BITS.css" rel="stylesheet" type="text/css" />
	
	<script type = "text/javascript">
    //insert javascript code
	</script>
</head>
<body>
    <b>About BITS</b>
    <p>
	    This web application is the home of the BASIS Integrated Tool Suite (BITS).<br>
	    BITS is a tool that hosts various custom applications, tools and email<br>
	    notifications that the internal BASIS team members use. Please select a tab<br>
	    to begin.
    </p><br>
    <a href="../index.jsp" target="_parent">Back</a>
</body>
</html>
