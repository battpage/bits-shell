package com.dev.servlet.session;

import java.io.IOException;
import java.io.PrintWriter;
 
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
 
/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    
    /*!!Insert Utility to grab username and passwords!!*/
    //User credential util as well as event handler for tabs here
    private final String userID = "bguzman";
    private final String password = "pwds";
    
    private final String userID2 = "admin";
    private final String password2 = "admin";
    
    private final String userID3 = "sbuhr";
    private final String password3 = "pwds";
    
    private final String userID4 = "pclarke";
    private final String password4 = "pwds";
 
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
 
        // get request parameters for userID and password
        String user = request.getParameter("user");
        String pwd = request.getParameter("pwd");
         
	    //modify section to hit database to check if user exists!!!
        if((userID.equals(user) && password.equals(pwd)) || (userID2.equals(user) && password2.equals(pwd)) || (userID3.equals(user) && password3.equals(pwd)) || (userID4.equals(user) && password4.equals(pwd))) {
            HttpSession session = request.getSession();
            
            if (userID.equals(user)) {
            	session.setAttribute("user", userID);	
            }
            if (userID2.equals(user)) {
            	session.setAttribute("user", userID2);	
            }
            if (userID3.equals(user)) {
            	session.setAttribute("user", userID3);	
            }
            if (userID4.equals(user)) {
            	session.setAttribute("user", userID4);	
            }
            
            //setting session to expire in 30 mins
            session.setMaxInactiveInterval(30*60);
            Cookie userName = new Cookie("user", user);
            userName.setMaxAge(30*60);
            response.addCookie(userName);
            response.sendRedirect("index.jsp");
        }else{
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/login.html");
            PrintWriter out= response.getWriter();
            rd.include(request, response);
            out.println("<font color=red>Either user name or password is wrong.</font>");
        }
    }
}