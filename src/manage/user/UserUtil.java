package manage.user;

import javax.servlet.http.Cookie;

public class UserUtil {
	public static String getUser(Cookie[] c) {
		String userName = null;
		if(c !=null){
			for(Cookie cookie : c){
			    if(cookie.getName().equals("user")) userName = cookie.getValue();
			}
		}
		return userName;
	}
}
